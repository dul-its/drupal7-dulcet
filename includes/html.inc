<?php
/**
 * @file
 * Theme and preprocess functions for html. SCA CUSTOM
 */

function dulcet_preprocess_html(&$vars) {
	// suggest the use of the 'embed' template that removes
	// all of the decorations (megamenus, footer, etc)
	if (isset($_GET['response_type']) && $_GET['response_type'] == 'embed') {
		$vars['theme_hook_suggestions'][] = 'html__embed';
	}


	// Load external JS Navigation
    drupal_add_js('//library.duke.edu/masthead/load-masthead.js.php?fixed=false&width=1280', array('group' => -201, 'weight' => -1000, 'preprocess' => FALSE));


	drupal_add_css(path_to_theme() . '/css/ie-lte-8.css', array('group' => CSS_THEME, 'weight' => 115, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
	$alias = drupal_get_path_alias();

	# fetch the pages that require Simile
	$simile_pages = preg_split("/\s+/", theme_get_setting('simile_pages', 'dulcet'));

	// Detect IE
	$findie11 = strpos($_SERVER["HTTP_USER_AGENT"], 'Trident');
	if ($findie11 !== false) {
    	$mybrowser = "IE";
	}
	elseif (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    	$mybrowser = "IE";
	}
	else {
    	$mybrowser = "NOT-IE";
	}

	// Space Finder (places to study replacement based on itemsjs)
	if ($alias == "using/library-spaces") {
		$GLOBALS['conf']['cache'] = FALSE;
    drupal_add_js('/sites/all/libraries/itemsjs/itemsjs.js', array('group' => -101, 'weight' => -1000, 'preprocess' => TRUE));
		drupal_add_js('/sites/all/libraries/vue/vue.min.v2.5.22.js', array('preprocess' => TRUE));
		drupal_add_js('/sites/all/libraries/itemsjs/spacefinder.js', array('preprocess' => FALSE));
	}
}

/**
 * The "linked_logo_img" needs to be modified to ensure the image's parent
 * link points to 'library.duke.edu'
 */
function dulcet_process_html(&$variables) {
	# create a variable for the hostname, making it available to html.tpl.php variation(s)
	$variables['dul_hostname'] = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];
}
